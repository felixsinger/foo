#include <common/register.h>
#include <platform/332691/chapter_21.h>

const struct register_t summary_of_ish_pci_configuration_registers[] = {
	{ 0x0, 4, 0, 0x0, "DEVVENDID", "Device and Vendor ID Register" },
	{ 0x4, 4, 0, 0x100000, "STATUSCOMMAND", "Status and Command" },
	{ 0x8, 4, 0, 0x0, "REVCLASSCODE", "Revision ID and Class Code" },
	{ 0xc, 4, 0, 0x0, "CLLATHEADERBIST", "Cache Line Latency Header and BIST" },
	{ 0x10, 4, 0, 0x0, "BAR", "Memory Base Address Register" },
	{ 0x14, 4, 0, 0x0, "BAR_HIGH", "Memory Base Address Register High" },
	{ 0x18, 4, 0, 0x0, "BAR1", "Memory Base Address 1" },
	{ 0x1c, 4, 0, 0x0, "BAR1_HIGH", "Memory Base Address 1 High" },
	{ 0x2c, 4, 0, 0x0, "SUBSYSTEMID", "Subsystem Vendor and Subsystem ID" },
	{ 0x30, 4, 0, 0x0, "EXPANSION_ROM_BASEADDR", "Expansion ROM base address" },
	{ 0x34, 4, 0, 0x80, "CAPABILITYPTR", "Capabilities Pointer" },
	{ 0x3c, 4, 0, 0x100, "INTERRUPTREG", "Interrupt" },
	{ 0x80, 4, 0, 0x48030001, "POWERCAPID", "PowerManagement Capability ID" },
	{ 0x84, 4, 0, 0x8, "PMECTRLSTATUS", "Power Management Control and Status" },
	{ 0xa0, 4, 0, 0x0, "GEN_REGRW1", "General Purpose Read Write Register1" },
	{ 0xa4, 4, 0, 0x0, "GEN_REGRW2", "General Purpose Read Write Register2" },
	{ 0xa8, 4, 0, 0x0, "GEN_REGRW3", "General Purpose Read Write Register3" },
	{ 0xac, 4, 0, 0x0, "GEN_REGRW4", "General Purpose Read Write Register4" },
	{ 0xc0, 4, 0, 0x0, "GEN_INPUT_REG", "General Purpose Input Register" },
};

const struct register_t summary_of_ish_mmio_registers[] = {
	{ 0x34, 4, 0, 0x0, "ISH_HOST_FWSTS", "ISH Host firmware status" },
	{ 0x38, 4, 0, 0x0, "HOST_COMM", "Host Communication" },
	{ 0x48, 4, 0, 0x0, "HOST2ISH_DOORBELL", NULL },
	{ 0x54, 4, 0, 0x0, "ISH2HOST_DOORBELL", "ISH-to-Host DoorBell" },
	{ 0x60, 4, 0, 0x0, "ISH2HOST_MSG", "Message from ISH to HOST" },
	{ 0xe0, 4, 0, 0x0, "HOST2ISH_MSG1", "Message from HOST to ISH" },
	{ 0x360, 4, 0, 0x0, "REMAP", "ISH Remap" },
	{ 0x6d0, 4, 0, 0x8, "IPC_d0i3C_reg", "D0i3 Control" },
};
