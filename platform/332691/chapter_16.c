#include <common/register.h>
#include <platform/332691/chapter_16.h>

const struct register_t summary_of_ide_redirect_pci_configuration_d22_f2_registers[] = {
	{ 0x0, 4, 1, 0x0, "IDE_HOST_DID_VID", "Device ID and Vendor ID" },
	{ 0x4, 4, 0, 0xb00000, "IDE_HOST_STS_CMD", "Status and Command" },
	{ 0x8, 4, 0, 0x1018500, "IDE_HOST_CC_RID", "Class Code and Revision ID" },
	{ 0xc, 4, 0, 0x800000, "IDE_HOST_BIST_HTYPE_LT_CLS", "BIST, Header Type, Latency Timer, and Cache Line Size" },
	{ 0x10, 4, 0, 0x1, "IDE_HOST_PCMDIOBAR", "IDE Primary Command Block IO BAR" },
	{ 0x14, 4, 0, 0x1, "IDE_HOST_PCTLIOBAR", "IDE Primary Control Block IO BAR" },
	{ 0x18, 4, 0, 0x1, "IDE_HOST_SCMDIOBAR", "IDE Secondary Command Block IO BAR" },
	{ 0x1c, 4, 0, 0x1, "IDE_HOST_SCTLIOBAR", "IDE Secondary Control Block IO BAR" },
	{ 0x20, 4, 0, 0x1, "IDE_HOST_BMIOBAR", "IDE Bus Master Block IO BAR" },
	{ 0x2c, 4, 0, 0x8086, "IDE_HOST_SID_SVID", "Subsystem ID and Subsystem Vendor ID" },
	{ 0x34, 4, 0, 0x40, "IDE_HOST_CAPP", "Capabilities List Pointer" },
	{ 0x3c, 4, 0, 0x0, "IDE_HOST_MAXL_MING_INTP_INTL", "Maximum Latency, Minimum Grant, Interrupt Pin and Interrupt Line" },
	{ 0x40, 4, 0, 0x805005, "IDE_HOST_MSIMC_MSINP_MSICID", "MSI Message Control, Next Pointer and Capability ID" },
	{ 0x44, 4, 0, 0x0, "IDE_HOST_MSIMA", "MSI Message Address" },
	{ 0x48, 4, 0, 0x0, "IDE_HOST_MSIMUA", "MSI Message Upper Address" },
	{ 0x4c, 4, 0, 0x0, "IDE_HOST_MSIMD", "MSI Message Data" },
	{ 0x50, 4, 0, 0x230001, "IDE_HOST_PMCAP_PMNP_PMCID", "Power Management Capabilities, Next Pointer and Capability ID" },
	{ 0x54, 4, 0, 0x8, "IDE_HOST_PMD_PMCSRBSE_PMCSR", "Power Management Data, Control/Status Register Bridge Support Extensions, Control and Status" },
};
