#include <common/register.h>
#include <platform/332691/chapter_15.h>

const struct register_t summary_of_management_engine_interface_pci_configuration_registers[] = {
	{ 0x0, 4, 1, 0x0, "HECI1_ID", "Identifiers" },
	{ 0x4, 2, 0, 0x0, "HECI1_CMD", "Command" },
	{ 0x6, 2, 0, 0x10, "HECI1_STS", "Status" },
	{ 0x8, 4, 0, 0x7800000, "HECI1_RID_CC", "Revision ID and Class Code" },
	{ 0xc, 1, 0, 0x0, "HECI1_CLS", "Cache Line Size" },
	{ 0xd, 1, 0, 0x0, "HECI1_MLT", "Master Latency Timer" },
	{ 0xe, 1, 0, 0x80, "HECI1_HTYPE", "Header Type" },
	{ 0xf, 1, 0, 0x0, "HECI1_BIST", "Built In Self-Test" },
	{ 0x10, 4, 0, 0x4, "HECI1_MMIO_MBAR_LO", "MMIO Base Address Low" },
	{ 0x14, 4, 0, 0x0, "HECI1_MMIO_MBAR_HI", "MMIO Base Address High" },
	{ 0x2c, 4, 0, 0x0, "HECI1_SS", "Sub System Identifiers" },
	{ 0x34, 4, 0, 0x50, "HECI1_CAP", "Capabilities Pointer" },
	{ 0x3c, 2, 0, 0x100, "HECI1_INTR", "Interrupt Information" },
	{ 0x3e, 1, 0, 0x0, "HECI1_MGNT", "Minimum Grant" },
	{ 0x3f, 1, 0, 0x0, "HECI1_MLAT", "Maximum Latency" },
	{ 0x40, 4, 0, 0x0, "HFSTS1", "Host Firmware Status Register 1" },
	{ 0x48, 4, 0, 0x0, "HFSTS2", "Host Firmware Status Register 2" },
	{ 0x4c, 4, 0, 0x0, "HECI1_H_GS1", "Host General Status" },
	{ 0x50, 2, 0, 0x8c01, "HECI1_PID", "PCI Power Management Capability ID" },
	{ 0x52, 2, 0, 0x4003, "HECI1_PC", "PCI Power Management Capabilities" },
	{ 0x54, 2, 0, 0x8, "HECI1_PMCS", "PCI Power Management Control and Status" },
	{ 0x60, 4, 0, 0x0, "HFSTS3", "Host Firmware Status Register 3" },
	{ 0x64, 4, 0, 0x0, "HFSTS4", "Host Firmware Status Register 4" },
	{ 0x68, 4, 0, 0x0, "HFSTS5", "Host Firmware Status Register 5" },
	{ 0x6c, 4, 0, 0x0, "HFSTS6", "Host Firmware Status Register 6" },
	{ 0x70, 4, 0, 0x0, "HECI1_H_GS2", "Host General Status 2" },
	{ 0x74, 4, 0, 0x0, "HECI1_H_GS3", "Host General Status 3" },
	{ 0x8c, 2, 0, 0x5, "HECI1_MID", "Message Signaled Interrupt Identifiers" },
	{ 0x8e, 2, 0, 0x80, "HECI1_MC", "Message Signaled Interrupt Message Control" },
	{ 0x90, 4, 0, 0x0, "HECI1_MA", "Message Signaled Interrupt Message Address" },
	{ 0x94, 4, 0, 0x0, "HECI1_MUA", "Message Signaled Interrupt Upper Address" },
	{ 0x98, 2, 0, 0x0, "HECI1_MD", "Message Signaled Interrupt Message Data" },
	{ 0xa0, 1, 0, 0x0, "HECI1_HIDM", "Interrupt Delivery Mode" },
};

const struct register_t summary_of_intel_mei_mmio_registers[] = {
	{ 0x800, 4, 0, 0x0, "HECI1_D0I3C", "D0i3 Control" },
};
