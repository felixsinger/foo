#include <common/register.h>
#include <platform/332691/chapter_29.h>

const struct register_t summary_of_rtc_indexed_registers[] = {
	{ 0x0, 1, 0, 0x0, "Sec", "Seconds" },
	{ 0x1, 1, 0, 0x0, "Sec_Alarm", "Seconds Alarm" },
	{ 0x2, 1, 0, 0x0, "Minutes", "Minutes" },
	{ 0x3, 1, 0, 0x0, "Minutes_Alarm", "Minutes Alarm" },
	{ 0x4, 1, 0, 0x0, "Hours", "Hours" },
	{ 0x5, 1, 0, 0x0, "Hours_Alarm", "Hours Alarm" },
	{ 0x6, 1, 0, 0x0, "Day_of_Week", "Day of Week" },
	{ 0x7, 1, 0, 0x0, "Day_of_Month", "Day of Month" },
	{ 0x8, 1, 0, 0x0, "Month", "Month" },
	{ 0x9, 1, 0, 0x0, "Year", "Year" },
	{ 0xa, 1, 0, 0x20, "RTC_REGA", "Register A" },
	{ 0xb, 1, 0, 0x0, "Register_B", "Register B - General Configuration" },
	{ 0xc, 1, 0, 0x0, "Register_C", "Register C - Flag Register" },
	{ 0xd, 1, 0, 0x80, "Register_D", "Register D - Flag Register" },
};

const struct register_t summary_of_rtc_pcr_registers[] = {
	{ 0x3400, 4, 0, 0x0, "RC", "RTC Configuration" },
	{ 0x3414, 1, 0, 0x0, "BUC", "Backed Up Control" },
	{ 0x3f04, 4, 0, 0x0, "UIPSMI", "RTC Update In Progress SMI Control" },
};
