#include <common/register.h>
#include <platform/332691/chapter_18.h>

const struct register_t summary_of_xhci_configuration_registers[] = {
	{ 0x0, 2, 0, 0x8086, "VID", "Vendor ID" },
	{ 0x2, 2, 0, 0x0, "DID", "Device ID" },
	{ 0x4, 2, 0, 0x0, "CMD", "Command" },
	{ 0x6, 2, 0, 0x290, "STS", "Device Status" },
	{ 0x8, 1, 0, 0x0, "RID", "Revision ID" },
	{ 0x9, 1, 0, 0x30, "PI", "Programming Interface" },
	{ 0xa, 1, 0, 0x3, "SCC", "Sub Class Code" },
	{ 0xb, 1, 0, 0xc, "BCC", "Base Class Code" },
	{ 0xd, 1, 0, 0x0, "MLT", "Master Latency Timer" },
	{ 0xe, 1, 0, 0x0, "HT", "Header Type" },
	{ 0x10, 8, 0, 0x4, "MBAR", "Memory Base Address" },
	{ 0x2c, 2, 0, 0x0, "SSVID", "USB Subsystem Vendor ID" },
	{ 0x2e, 2, 0, 0x0, "SSID", "USB Subsystem ID" },
	{ 0x34, 1, 0, 0x70, "CAP_PTR", "Capabilities Pointer" },
	{ 0x3c, 1, 0, 0x0, "ILINE", "Interrupt Line" },
	{ 0x3d, 1, 0, 0x0, "IPIN", "Interrupt Pin" },
	{ 0x40, 4, 0, 0x1fd, "XHCC1", "XHC System Bus Configuration 1" },
	{ 0x44, 4, 0, 0x3c000, "XHCC2", "XHC System Bus Configuration 2" },
	{ 0x50, 4, 0, 0x0, "XHCLKGTEN", "Clock Gating" },
	{ 0x58, 4, 0, 0x0, "AUDSYNC", "Audio Time Synchronization" },
	{ 0x60, 1, 0, 0x30, "SBRN", "Serial Bus Release Number" },
	{ 0x61, 1, 0, 0x60, "FLADJ", "Frame Length Adjustment" },
	{ 0x62, 1, 0, 0x0, "BESL", "Best Effort Service Latency" },
	{ 0x70, 1, 0, 0x1, "PM_CID", "PCI Power Management Capability ID" },
	{ 0x71, 1, 0, 0x80, "PM_NEXT", "Next Item Pointer #1" },
	{ 0x72, 2, 0, 0xc1c2, "PM_CAP", "Power Management Capabilities" },
	{ 0x74, 2, 0, 0x8, "PM_CS", "Power Management Control/Status" },
	{ 0x80, 1, 0, 0x5, "MSI_CID", "Message Signaled Interrupt CID" },
	{ 0x81, 1, 0, 0x0, "MSI_NEXT", "Next item pointer" },
	{ 0x82, 2, 0, 0x86, "MSI_MCTL", "Message Signaled Interrupt Message Control" },
	{ 0x84, 4, 0, 0x0, "MSI_MAD", "Message Signaled Interrupt Message Address" },
	{ 0x88, 4, 0, 0x0, "MSI_MUAD", "Message Signaled Interrupt Upper Address" },
	{ 0x8c, 2, 0, 0x0, "MSI_MD", "Message Signaled Interrupt Message Data" },
	{ 0x94, 4, 0, 0x1400010, "VSHDR", "Vendor Specific Header" },
	{ 0xa2, 2, 0, 0x8, "PCE_REG", "Power Control Enable" },
	{ 0xa4, 4, 0, 0x2000, "HSCFG2", "High Speed Configuration 2" },
	{ 0xb0, 4, 0, 0x0, "U2OCM", "XHCI USB2 Overcurrent Pin Mapping N" },
	{ 0xd0, 4, 0, 0x0, "U3OCM", "XHCI USB3 Overcurrent Pin Mapping N" },
};

const struct register_t summary_of_xhci_memory_mapped_registers[] = {
	{ 0x0, 1, 0, 0x20, "CAPLENGTH", "Capability Registers Length" },
	{ 0x2, 2, 0, 0x100, "HCIVERSION", "Host Controller Interface Version Number" },
	{ 0x4, 4, 0, 0x1a000840, "HCSPARAMS1", "Structural Parameters 1" },
	{ 0x8, 4, 0, 0x14200054, "HCSPARAMS2", "Structural Parameters 2" },
	{ 0xc, 4, 0, 0x40001, "HCSPARAMS3", "Structural Parameters 3" },
	{ 0x10, 4, 0, 0x200077c1, "HCCPARAMS", "Capability Parameters" },
	{ 0x14, 4, 0, 0x3000, "DBOFF", "Doorbell Offset" },
	{ 0x18, 4, 0, 0x2000, "RTSOFF", "Runtime Register Space Offset" },
	{ 0x80, 4, 0, 0x0, "USBCMD", "USB Command" },
	{ 0x84, 4, 0, 0x1, "USBSTS", "USB Status" },
	{ 0x88, 4, 0, 0x1, "PAGESIZE", "Page Size" },
	{ 0x94, 4, 0, 0x0, "DNCTRL", "Device Notification Control" },
	{ 0x98, 4, 0, 0x0, "CRCR_LO", "Command Ring Low" },
	{ 0x9c, 4, 0, 0x0, "CRCR_HI", "Command Ring High" },
	{ 0xb0, 4, 0, 0x0, "DCBAAP_LO", "Device Context Base Address Array Pointer Low" },
	{ 0xb4, 4, 0, 0x0, "DCBAAP_HI", "Device Context Base Address Array Pointer High" },
	{ 0x480, 4, 0, 0x2a0, "PORTSCN", "Port N Status and Control USB2" },
	{ 0x484, 4, 0, 0x0, "PORTPMSCN", "Port Power Management Status and Control USB2" },
	{ 0x48c, 4, 0, 0x0, "PORTHLPMCN", "Port N Hardware LPM Control Register" },
	{ 0x580, 4, 0, 0x2a0, "PORTSCXUSB3", "Port Status and Control USB3" },
	{ 0x584, 4, 0, 0x0, "PORTPMSCX", "Port Power Management Status and Control USB3" },
	{ 0x588, 4, 0, 0x0, "PORTLIX", "USB3 Port X Link Info" },
	{ 0x2000, 4, 0, 0x0, "MFINDEX", "Microframe Index" },
	{ 0x2020, 4, 0, 0x0, "IMANx", "Interrupter x Management" },
	{ 0x2024, 4, 0, 0xfa0, "IMODx", "Interrupter x Moderation" },
	{ 0x2028, 4, 0, 0x0, "ERSTSZx", "Event Ring Segment Table Size x" },
	{ 0x2030, 4, 0, 0x0, "ERSTBA_LOx", "Event Ring Segment Table Base Address Low x" },
	{ 0x2034, 4, 0, 0x0, "ERSTBA_HIx", "Event Ring Segment Table Base Address High x" },
	{ 0x2038, 4, 0, 0x0, "ERDP_LOx", "Event Ring Dequeue Pointer Low x" },
	{ 0x203c, 4, 0, 0x0, "ERDP_HIx", "Event Ring Dequeue Pointer High x" },
	{ 0x3000, 4, 0, 0x0, "DBx", "Door Bell x" },
	{ 0x8000, 4, 0, 0x2000802, "XECP_SUPP_USB2_0", "XECP_SUPP_USB2_0" },
	{ 0x8004, 4, 0, 0x20425355, "XECP_SUPP_USB2_1", "XECP_SUPP_USB2_1" },
	{ 0x8008, 4, 0, 0x30181001, "XECP_SUPP_USB2_2", "XECP_SUPP_USB2_2" },
	{ 0x8010, 4, 0, 0xc0021, "XECP_SUPP_USB2_3", "XECP_SUPP_USB2_3 (Full Speed)" },
	{ 0x8014, 4, 0, 0x5dc0012, "XECP_SUPP_USB2_4", "XECP_SUPP_USB2_4 (Low Speed)" },
	{ 0x8018, 4, 0, 0x1e00023, "XECP_SUPP_USB2_5", "XECP_SUPP_USB2_5 (High Speed)" },
	{ 0x8020, 4, 0, 0x3001402, "XECP_SUPP_USB3_0", "XECP_SUPP_USB3_0" },
	{ 0x8024, 4, 0, 0x20425355, "XECP_SUPP_USB3_1", "XECP_SUPP_USB3_1" },
	{ 0x8028, 4, 0, 0x30000a11, "XECP_SUPP_USB3_2", "XECP_SUPP_USB3_2" },
	{ 0x8030, 4, 0, 0x4e00121, "XECP_SUPP_USB3_3", "XECP_SUPP_USB3_3" },
	{ 0x8034, 4, 0, 0x9c00122, "XECP_SUPP_USB3_4", "XECP_SUPP_USB3_4" },
	{ 0x8038, 4, 0, 0x13800123, "XECP_SUPP_USB3_5", "XECP_SUPP_USB3_5" },
	{ 0x803c, 4, 0, 0x50134, "XECP_SUPP_USB3_6", "XECP_SUPP_USB3_6" },
	{ 0x8040, 4, 0, 0x5b10125, "XECP_SUPP_USB3_7", "XECP_SUPP_USB3_7" },
	{ 0x8044, 4, 0, 0xb630126, "XECP_SUPP_USB3_8", "XECP_SUPP_USB3_8" },
	{ 0x8048, 4, 0, 0x16c60127, "XECP_SUPP_USB3_9", "XECP_SUPP_USB3_9" },
	{ 0x8094, 4, 0, 0x100, "HOST_CTRL_SCH_REG", "Host Control Scheduler" },
	{ 0x80a4, 4, 0, 0x2dff90, "PMCTRL_REG", "Power Management Control" },
	{ 0x80b0, 4, 0, 0x1037f, "HOST_CTRL_MISC_REG", "HOST_CTRL_MISC_REG" },
	{ 0x80b4, 4, 0, 0x0, "HOST_CTRL_MISC_REG2", "HOST_CTRL_MISC_REG2" },
	{ 0x80b8, 4, 0, 0x0, "SSPE_REG", "SSPE_REG" },
	{ 0x80d8, 4, 0, 0x800, "DUAL_ROLE_CFG_REG0", "DEVICE MODE CONTROL REG 0" },
	{ 0x80e0, 4, 0, 0x808d3ca0, "AUX_CTRL_REG1", "AUX Power Management Control" },
	{ 0x80ec, 4, 0, 0x18010000, "HOST_CTRL_PORT_LINK_REG", "SuperSpeed Port Link Control" },
	{ 0x80f0, 4, 0, 0x310803a0, "USB2_LINK_MGR_CTRL_REG1", "USB2 Port Link Control 1" },
	{ 0x80fc, 4, 0, 0x8003, "USB2_LINK_MGR_CTRL_REG4", "USB2 Port Link Control 4" },
	{ 0x8140, 4, 0, 0xa019132, "PWR_SCHED_CTRL0", "Power Scheduler Control-0" },
	{ 0x8144, 4, 0, 0x33f, "PWR_SCHED_CTRL2", "Power Scheduler Control-2" },
	{ 0x8154, 4, 0, 0x1390206, "AUX_CTRL_REG2", "AUX Power Management Control" },
	{ 0x8164, 4, 0, 0xfc, "USB2_PHY_PMC", "USB2 PHY Power Management Control" },
	{ 0x816c, 4, 0, 0x400, "XHCI_AUX_CCR", "xHCI Aux Clock Control Register" },
	{ 0x8174, 4, 0, 0x40047d, "XLTP_LTV1", "xHC Latency Tolerance Parameters - LTV Control" },
	{ 0x817c, 4, 0, 0x0, "XLTP_HITC", "xHC Latency Tolerance Parameters - High Idle Time Control" },
	{ 0x8180, 4, 0, 0x0, "XLTP_MITC", "xHC Latency Tolerance Parameters - Medium Idle Time Control" },
	{ 0x8184, 4, 0, 0x0, "XLTP_LITC", "xHC Latency Tolerance Parameters Low Idle Time Control" },
	{ 0x8190, 4, 0, 0x0, "XECP_CMDM_CTRL_REG2", "Command Manager Control 2" },
	{ 0x81b8, 4, 0, 0x20c8, "LFPSONCOUNT_REG", "LFPSONCOUNT_REG" },
	{ 0x81c4, 4, 0, 0x0, "USB2PMCTRL_REG", "USB2 PM Control" },
	{ 0x8420, 4, 0, 0x0, "STRAP2_REG", "STRAP2_REG" },
	{ 0x846c, 4, 0, 0x2201, "USBLEGSUP", "USB Legacy Support Capability" },
	{ 0x84f4, 4, 0, 0x3c6, "PDO_CAPABILITY", "Port Disable Override capability register" },
	{ 0x84f8, 4, 0, 0x0, "USB2PDO", "USB2 Port Disable Override" },
	{ 0x84fc, 4, 0, 0x0, "USB3PDO", "USB3 Port Disable Override" },
	{ 0x8700, 4, 0, 0x5100a, "DCID", "Debug Capability ID Register" },
	{ 0x8900, 4, 0, 0xc5, "SSIC_PROFILE_CAPABILITY_ID_REG", "SSIC Local and Remote Profile Registers Capability ID register" },
	{ 0x8904, 4, 0, 0xc00000, "PORT1_REGISTER_ACCESS_CONTROL", "SSIC Port N Register Access Control" },
	{ 0x8908, 4, 0, 0x0, "PORT1_REGISTER_ACCESS_STATUS", "SSIC Port N Register Access Status" },
	{ 0x890c, 4, 0, 0x0, "PORT1_PROFILE_ATTRIBUTES_REG0", NULL },
	{ 0x8a14, 4, 0, 0xc00000, "PORT2_REGISTER_ACCESS_CONTROL", "SSIC Port N Register Access Control" },
	{ 0x8a18, 4, 0, 0x0, "PORT2_REGISTER_ACCESS_STATUS", "SSIC Port N Register Access Status" },
	{ 0x8a1c, 4, 0, 0x0, "PORT2_PROFILE_ATTRIBUTES_REG0", NULL },
	{ 0x8e10, 4, 0, 0xc9, "GLOBAL_TIME_SYNC_CAP_REG", "Global Time Sync Capability" },
	{ 0x8e14, 4, 0, 0x0, "GLOBAL_TIME_SYNC_CTRL_REG", "Global Time Sync Control" },
	{ 0x8e18, 4, 0, 0x0, "MICROFRAME_TIME_REG", "Microframe Time (Local Time)" },
	{ 0x8e20, 4, 0, 0x0, "ALWAYS_RUNNING_TIME_LOW", "Always Running Time (ART) Low" },
	{ 0x8e24, 4, 0, 0x0, "ALWAYS_RUNNING_TIME_HIGH", "Always Running Time (ART) High" },
};
