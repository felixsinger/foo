#include <common/register.h>
#include <platform/332691/chapter_22.h>

const struct register_t summary_of_8254_timer_registers[] = {
	{ 0x40, 1, 0, 0xc4, "C0_ITSBFR", "Counter 0 - Interval Timer Status Byte Format Register" },
	{ 0x40, 1, 0, 0x0, "C0_CAPR", "Counter 0 - Counter Access Ports Register" },
	{ 0x42, 1, 0, 0x0, "C2_ITSBFR", "Counter 2 - Interval Timer Status Byte Format Register" },
	{ 0x42, 1, 0, 0x0, "C2_CAPR", "Counter 2 - Counter Access Ports Register" },
	{ 0x43, 1, 0, 0x0, "TCW", "Timer Control Word Register" },
	{ 0x43, 1, 0, 0xc0, "RBC", "Read Back Command" },
	{ 0x43, 1, 0, 0x0, "CLC", "Counter Latch Command" },
};
