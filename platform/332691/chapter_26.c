#include <common/register.h>
#include <platform/332691/chapter_26.h>

const struct register_t summary_of_hpet_memory_mapped_registers[] = {
	{ 0x0, 8, 0, 0x27bc86b8086a701, "GEN_CAP_ID", "General Capabilities and ID Register" },
	{ 0x10, 8, 0, 0x0, "GEN_CFG", "General Config Register" },
	{ 0x20, 8, 0, 0x0, "GEN_INT_STS", "General Interrupt Status Register" },
	{ 0xf0, 8, 0, 0x0, "MAIN_CNTR", "Main Counter Value" },
	{ 0x100, 8, 0, 0xf0000000008030, "TMRn_CNF_CAP", "Timer n Config and Capabilities" },
	{ 0x108, 8, 0, 0xffffffffffffffff, "TMRn_CMP_VAL", "Timer n Comparator Value" },
};
