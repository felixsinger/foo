#include <common/register.h>
#include <platform/332691/chapter_8.h>

const struct register_t summary_of_gbe_configuration_registers[] = {
	{ 0x0, 4, 0, 0x156f8086, "GBE_VID_DID", "GbE Vendor and Device Identification Register" },
	{ 0x4, 4, 0, 0x100000, "PCICMD_STS", "PCI Command & Status Register" },
	{ 0x8, 4, 0, 0x2000000, "RID_CC", "Revision Identification & Class Code Register" },
	{ 0xc, 4, 0, 0x0, "CLS_PLT_HEADTYP", "Cache Line Size Primary Latency Timer & Header Type Register" },
	{ 0x10, 4, 0, 0x0, "MBARA", "Memory Base Address Register A" },
	{ 0x2c, 4, 0, 0x8086, "DMI_CONFIG11", "Subsystem Vendor & Subsytem ID" },
	{ 0x30, 4, 0, 0x0, "ERBA", "Expansion ROM Base Address Register" },
	{ 0x34, 4, 0, 0xc8, "CAPP", "Capabilities List Pointer Register" },
	{ 0x3c, 4, 0, 0x100, "INTR_MLMG", "Interrupt Information & Maximum Latency/Minimum GrantRegister" },
	{ 0xa0, 4, 0, 0x0, "LANDISCTRL", "LAN Disable Control" },
	{ 0xa4, 4, 0, 0x0, "LOCKLANDIS", "Lock LAN Disable" },
	{ 0xa8, 4, 0, 0x0, "LTRCAP", "System Time Control High Register" },
	{ 0xc8, 4, 0, 0x23d001, "CLIST1_PMC", "Capabilities List and Power Managment Capabilities Register" },
	{ 0xcc, 4, 0, 0x0, "PMCS_DR", "PCI Power Management Control Status & Data Register" },
	{ 0xd0, 4, 0, 0x80e005, "CLIST2_MCTL", "Capabilities List 2 & Message Control Register" },
	{ 0xd4, 4, 0, 0x0, "MADDL", "Message Address Low Register" },
	{ 0xd8, 4, 0, 0x0, "MADDH", "Message Address High Register" },
	{ 0xdc, 4, 0, 0x0, "MDAT", "Message Data Register" },
};

const struct register_t summary_of_gbe_memory_mapped_i_o_registers[] = {
	{ 0x0, 4, 0, 0x0, "GBECSR_00", "Gigabit Ethernet Capabilities and Status" },
	{ 0x18, 4, 0, 0x0, "GBECSR_18", "Gigabit Ethernet Capabilities and Status" },
	{ 0x20, 4, 0, 0x10000000, "GBECSR_20", "Gigabit Ethernet Capabilities and Status" },
	{ 0xf00, 4, 0, 0x0, "GBECSR_F00", "Gigabit Ethernet Capabilities and Status" },
	{ 0xf10, 4, 0, 0xc, "GBECSR_F10", "Gigabit Ethernet Capabilities and Status F10" },
	{ 0x5400, 4, 0, 0x0, "GBECSR_5400", "Gigabit Ethernet Capabilities and Status" },
	{ 0x5404, 4, 0, 0x0, "GBECSR_5404", "Gigabit Ethernet Capabilities and Status" },
	{ 0x5800, 4, 0, 0x0, "GBECSR_5800", "Gigabit Ethernet Capabilities and Status" },
	{ 0x5b54, 4, 0, 0x0, "GBECSR_5B54", "Gigabit Ethernet Capabilities and Status" },
};
