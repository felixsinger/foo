#include <common/register.h>
#include <platform/332691/chapter_27.h>

const struct register_t summary_of_integrated_clock_controller_fw_accessible_registers[] = {
	{ 0x1000, 4, 0, 0x0, "TMCSRCCLK", "Timing Control SRC Clock" },
	{ 0x1004, 4, 0, 0x0, "TMCSRCCLK2", "Timing Control SRC Clock Register 2" },
	{ 0x1008, 4, 0, 0x0, "ENCCKRQ", "Enable Control CLKREQ" },
	{ 0x100c, 4, 0, 0x0, "MSKCKRQ", "Mask Control CLKREQ" },
	{ 0x1020, 4, 0, 0x0, "ICCSEC", "ICC Security" },
	{ 0x1024, 4, 0, 0x76543210, "CKRQMAPSRC", "CLKREQ Mapping to CLKOUT_SRC" },
	{ 0x1028, 4, 0, 0x76543210, "CKRQMAPSRC2", "CLKREQ Mapping to CLKOUT_SRC Register 2" },
	{ 0x102c, 4, 0, 0x0, "PM", "Power Management" },
	{ 0x1034, 4, 0, 0x0, "ICCDBG", "ICC Debug" },
	{ 0x2000, 4, 0, 0x0, "G2PLLCTRL", "USB3Gen2PCIe PLL Control" },
};
