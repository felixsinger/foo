#include <common/register.h>
#include <platform/332691/chapter_9.h>

const struct register_t summary_of_intel_trace_hub_configuration_registers[] = {
	{ 0x0, 4, 0, 0x9638086, "VID", "Vendor and Device Identification" },
	{ 0x4, 4, 0, 0x100000, "CMD", "Command and Status Register" },
	{ 0x8, 4, 0, 0x1300, "RID", "Revision ID" },
	{ 0xc, 4, 0, 0x0, "HT", "Header Type" },
	{ 0x10, 4, 0, 0x4, "MTB_LBAR", "MTB Low BAR" },
	{ 0x14, 4, 0, 0x0, "MTB_UBAR", "MTB Upper BAR" },
	{ 0x18, 4, 0, 0x4, "SW_LBAR", "SW Low BAR" },
	{ 0x1c, 4, 0, 0x0, "SW_UBAR", "SW Upper BAR" },
	{ 0x20, 4, 0, 0x4, "RTIT_LBAR", "RTIT Low BAR" },
	{ 0x24, 4, 0, 0x0, "RTIT_UBAR", "RTIT Upper BAR" },
	{ 0x34, 4, 0, 0x40, "CAP", "Capabilities Pointer" },
	{ 0x3c, 4, 0, 0x1ff, "INTL", "Interrupt Line and Interrupt Pin" },
	{ 0x40, 4, 0, 0x800005, "MSICID", "MSI Capability" },
	{ 0x44, 4, 0, 0x0, "MSILMA", "MSI Lower Message Address" },
	{ 0x48, 4, 0, 0x0, "MSIUMA", "MSI Upper Message Address" },
	{ 0x4c, 4, 0, 0x0, "MSIMD", "MSI Message Data" },
	{ 0x80, 4, 0, 0x1, "NPKDSC", "Device Specific Control and Device Specific Status" },
};
