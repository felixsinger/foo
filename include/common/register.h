#include <stdint.h>

#ifndef REGISTER_H
#define REGISTER_H

struct register_t {
	const uint16_t offset;
	const uint8_t size;
	const uint8_t has_straps;
	const uint64_t value_default;
	const char *description_short;
	const char *description_long;
};

#endif
