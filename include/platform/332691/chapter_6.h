#include <common/register.h>

#ifndef CHAPTER_6_H
#define CHAPTER_6_H

const struct register_t summary_of_smbus_configuration_registers[20];
const struct register_t summary_of_smbus_i_o_and_memory_mapped_i_o_registers[19];
const struct register_t summary_of_smbus_pcr_registers[3];

#endif
