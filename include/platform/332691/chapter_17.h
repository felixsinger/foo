#include <common/register.h>

#ifndef CHAPTER_17_H
#define CHAPTER_17_H

const struct register_t summary_of_keyboard_and_text_kt_pci_configuration_d22_f3_registers[17];
const struct register_t summary_of_keyboard_and_text_kt_additional_configuration_registers[1];

#endif
