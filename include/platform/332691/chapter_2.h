#include <common/register.h>

#ifndef CHAPTER_2_H
#define CHAPTER_2_H

const struct register_t summary_of_enhanced_spi_espi_pci_configuration_registers[17];
const struct register_t summary_of_espi_pcr_registers[6];

#endif
