#include <common/register.h>

#ifndef CHAPTER_10_H
#define CHAPTER_10_H

const struct register_t summary_of_uart_pci_configuration_registers[18];
const struct register_t summary_of_uart_memory_mapped_registers[29];
const struct register_t summary_of_uart_additional_registers[13];
const struct register_t summary_of_uart_dma_controller_registers[41];
const struct register_t summary_of_uart_pcr_registers[3];

#endif
