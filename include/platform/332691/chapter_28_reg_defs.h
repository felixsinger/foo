#ifndef CHAPTER_28_REG_DEFS_H
#define CHAPTER_28_REG_DEFS_H

#define REG_28_1_MICW1	0x20
#define REG_28_1_MOCW2	0x20
#define REG_28_1_MOCW3	0x20
#define REG_28_1_MICW2	0x21
#define REG_28_1_MICW3	0x21
#define REG_28_1_MICW4	0x21
#define REG_28_1_MOCW1	0x21
#define REG_28_1_SICW1	0xa0
#define REG_28_1_SOCW2	0xa0
#define REG_28_1_SOCW3	0xa0
#define REG_28_1_SICW2	0xa1
#define REG_28_1_SICW3	0xa1
#define REG_28_1_SICW4	0xa1
#define REG_28_1_SOCW1	0xa1
#define REG_28_1_ELCR1	0x4d0
#define REG_28_1_ELCR2	0x4d1

#define REG_28_2_PARC	0x3100
#define REG_28_2_PBRC	0x3101
#define REG_28_2_PCRC	0x3102
#define REG_28_2_PDRC	0x3103
#define REG_28_2_PERC	0x3104
#define REG_28_2_PFRC	0x3105
#define REG_28_2_PGRC	0x3106
#define REG_28_2_PHRC	0x3107
#define REG_28_2_PIR0	0x3140
#define REG_28_2_PIR1	0x3142
#define REG_28_2_PIR2	0x3144
#define REG_28_2_PIR3	0x3146
#define REG_28_2_PIR4	0x3148
#define REG_28_2_GIC	0x31fc
#define REG_28_2_IPC0	0x3200
#define REG_28_2_IPC1	0x3204
#define REG_28_2_IPC2	0x3208
#define REG_28_2_IPC3	0x320c
#define REG_28_2_ITSSPRC	0x3300
#define REG_28_2_MMC	0x3334

#endif
