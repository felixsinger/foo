#include <common/register.h>

#ifndef CHAPTER_15_H
#define CHAPTER_15_H

const struct register_t summary_of_management_engine_interface_pci_configuration_registers[33];
const struct register_t summary_of_intel_mei_mmio_registers[1];

#endif
