#include <common/register.h>

#ifndef CHAPTER_20_H
#define CHAPTER_20_H

const struct register_t summary_of_thermal_reporting_configuration_registers[28];
const struct register_t summary_of_thermal_reporting_memory_mapped_registers[18];

#endif
