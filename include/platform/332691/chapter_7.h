#include <common/register.h>

#ifndef CHAPTER_7_H
#define CHAPTER_7_H

const struct register_t summary_of_spi_configuration_registers[8];
const struct register_t summary_of_spi_memory_mapped_registers[42];
const struct register_t summary_of_bios_flash_program_registers[3];

#endif
