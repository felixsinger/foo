#ifndef CHAPTER_29_REG_DEFS_H
#define CHAPTER_29_REG_DEFS_H

#define REG_29_1_SEC	0x0
#define REG_29_1_SECALARM	0x1
#define REG_29_1_MINUTES	0x2
#define REG_29_1_MINUTESALARM	0x3
#define REG_29_1_HOURS	0x4
#define REG_29_1_HOURSALARM	0x5
#define REG_29_1_DAYOFWEEK	0x6
#define REG_29_1_DAYOFMONTH	0x7
#define REG_29_1_MONTH	0x8
#define REG_29_1_YEAR	0x9
#define REG_29_1_RTCREGA	0xa
#define REG_29_1_REGISTERB	0xb
#define REG_29_1_REGISTERC	0xc
#define REG_29_1_REGISTERD	0xd

#define REG_29_2_RC	0x3400
#define REG_29_2_BUC	0x3414
#define REG_29_2_UIPSMI	0x3f04

#endif
