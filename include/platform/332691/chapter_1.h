#include <common/register.h>

#ifndef CHAPTER_1_H
#define CHAPTER_1_H

const struct register_t summary_of_lpc_configuration_registers[22];
const struct register_t summary_of_lpc_pcr_registers[1];

#endif
