#include <common/register.h>

#ifndef CHAPTER_25_H
#define CHAPTER_25_H

const struct register_t summary_of_gpio_community_0_registers[123];
const struct register_t summary_of_gpio_community_1_registers[353];
const struct register_t summary_of_gpio_community_2_registers[36];
const struct register_t summary_of_gpio_community_3_registers[39];

#endif
