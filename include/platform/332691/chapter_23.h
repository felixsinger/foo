#include <common/register.h>

#ifndef CHAPTER_23_H
#define CHAPTER_23_H

const struct register_t summary_of_apic_indirect_registers[122];
const struct register_t summary_of_advanced_programmable_interrupt_controller_apic_registers[3];

#endif
