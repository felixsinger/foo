#ifndef CHAPTER_3_REG_DEFS_H
#define CHAPTER_3_REG_DEFS_H

#define REG_3_1_PCIID	0x0
#define REG_3_1_PCICMD	0x4
#define REG_3_1_PCIRID	0x8
#define REG_3_1_PCICC	0x9
#define REG_3_1_PCIHTYPE	0xe
#define REG_3_1_SBREGBAR	0x10
#define REG_3_1_SBREGBARH	0x14
#define REG_3_1_PCIHSS	0x2c
#define REG_3_1_VBDF	0x50
#define REG_3_1_EBDF	0x52
#define REG_3_1_RCFG	0x54
#define REG_3_1_HPTC	0x60
#define REG_3_1_IOAC	0x64
#define REG_3_1_IBDF	0x6c
#define REG_3_1_HBDF	0x70
#define REG_3_1_SBREGPOSTED0	0x80
#define REG_3_1_SBREGPOSTED1	0x84
#define REG_3_1_SBREGPOSTED2	0x88
#define REG_3_1_SBREGPOSTED3	0x8c
#define REG_3_1_SBREGPOSTED4	0x90
#define REG_3_1_SBREGPOSTED5	0x94
#define REG_3_1_SBREGPOSTED6	0x98
#define REG_3_1_SBREGPOSTED7	0x9c
#define REG_3_1_DISPBDF	0xa0
#define REG_3_1_ICCOS	0xa4
#define REG_3_1_EPMASK0	0xb0
#define REG_3_1_EPMASK1	0xb4
#define REG_3_1_EPMASK2	0xb8
#define REG_3_1_EPMASK3	0xbc
#define REG_3_1_EPMASK4	0xc0
#define REG_3_1_EPMASK5	0xc4
#define REG_3_1_EPMASK6	0xc8
#define REG_3_1_EPMASK7	0xcc
#define REG_3_1_SBIADDR	0xd0
#define REG_3_1_SBIDATA	0xd4
#define REG_3_1_SBISTAT	0xd8
#define REG_3_1_SBIRID	0xda
#define REG_3_1_SBIEXTADDR	0xdc
#define REG_3_1_P2SBC	0xe0
#define REG_3_1_PCE	0xe4

#endif
