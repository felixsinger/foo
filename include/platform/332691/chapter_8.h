#include <common/register.h>

#ifndef CHAPTER_8_H
#define CHAPTER_8_H

const struct register_t summary_of_gbe_configuration_registers[18];
const struct register_t summary_of_gbe_memory_mapped_i_o_registers[9];

#endif
